<?php

use Carbon\Carbon;

class MainController extends BaseController {

    protected $activity;
    protected $brand;
    protected $attendee;

    public function __construct(Activity $activity, Brand $brand, Attendee $attendee)
    {
        parent::__construct();

        $this->activity = $activity;
        $this->brand = $brand;
        $this->attendee = $attendee;
    }

	public function getIndex()
	{
        $nowTime = Carbon::now();
        $myAttending = 0;
        $myAreadyAttending = 0;

		$tongdaoActivityCount = $this->activity->common()->tongdaoclub()
                                  ->where('start_time', '>', time())
                                  ->count();

        $brandCount = $this->brand->where('status', '=', 0)
                                  ->count();

        $myAttendeesQuery = $this->attendee
                                 ->where('member_id', '=', 2)
                                 ->get();
        
        foreach($myAttendeesQuery as $var)
        { 
            if($nowTime->lt(Carbon::createFromFormat('Y-m-d H:i:s', $var->activity()->first()->start_time)))
            {
               $myAttending ++; 
            }
            else
            {
                $myAreadyAttending ++;
            }    
        }

        $vo = array(
            'tongdaoActivityCount'    => $tongdaoActivityCount, 
            'brandCount' => $brandCount, 
            'myAttending' => $myAttending,
            'myAreadyAttending' => $myAreadyAttending,
        );

        return View::make('site/activity/index', $vo);

     //   echo dd($vo);
	}

}