<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Creates the activity table
        Schema::create('activities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('brand_id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->string('address');
            $table->string('time_desc');
            $table->dateTime('start_time');
            $table->integer('level')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activities');
	}

}
