<?php

class MembersTableSeeder extends Seeder {

    public function run()
    {
        DB::table('members')->delete();


        $members = array(
            array(
                'name'      => 'paul',
                'email'      => 'paul@gmail.com',
                'wechat_openid'   => Hash::make('wang'),
                'nick_name'   => 'peng.paul',
                'headimg_url' => 'sh.jpg',
                'subscribe_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'name'      => 'lvzhi',
                'email'      => 'zak@gmail.com',
                'wechat_openid'   => Hash::make('zak'),
                'nick_name'   => 'zak',
                'headimg_url' => 'sh.jpg',
                'subscribe_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('members')->insert( $members );
    }

}
