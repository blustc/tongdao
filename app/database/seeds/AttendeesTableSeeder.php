<?php

class AttendeesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('attendees')->delete();


        $attendees = array(
            array(
                'member_id'      => 1,
                'activity_id'      => 1,
                'attend_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'member_id'      => 1,
                'activity_id'      => 2,
                'attend_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'member_id'      => 2,
                'activity_id'      => 1,
                'attend_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'member_id'      => 2,
                'activity_id'      => 2,
                'attend_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'member_id'      => 2,
                'activity_id'      => 3,
                'attend_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'member_id'      => 2,
                'activity_id'      => 5,
                'attend_time' => new DateTime,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('attendees')->insert( $attendees );
    }

}