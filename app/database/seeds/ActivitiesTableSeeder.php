<?php

class ActivitiesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('activities')->delete();


        $activities = array(
            array(
                'brand_id'      => 0,
                'title'      => '第一次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-20'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第二次线上发起的聚会',
                'description'   => '让我们来测试下吧2',
                'address'   => '又上海中心顶楼',
                'time_desc' => '2014.2.21 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-21'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第三次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-22'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第四次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-23'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第五次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-13'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第六次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-25'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第七次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-26'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'brand_id'      => 0,
                'title'      => '第八次线上发起的聚会',
                'description'   => '让我们来测试下吧',
                'address'   => '上海中心顶楼',
                'time_desc' => '2014.2.20 下午14:00 - 20:00',
                'start_time' => new DateTime('2014-02-27'),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('activities')->insert( $activities );
    }
}
