<?php

class BrandsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('brands')->delete();


        $brands = array(
            array(
                'title'      => '爱马仕礼遇专区',
                'description'      => '爱马仕礼遇专区',
                'logo'   => 'hermes.jpg',
                'brand_banner' => 'hermes_banner.jpg',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => '宝马品牌专区',
                'description'      => '宝马品牌专区·',
                'logo'   => 'bmw.jpg',
                'brand_banner' => 'bmw_banner.jpg',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            ),
            array(
                'title'      => '上海民生现代美术馆',
                'description'      => '上海民生现代美术馆',
                'logo'   => 'minshengart.jpg',
                'brand_banner' => 'minshengart_banner.jpg',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
            )
        );

        DB::table('brands')->insert( $brands );
    }

}
