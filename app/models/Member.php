<?php

use Robbo\Presenter\PresentableInterface;

Class Member extends extends Eloquent implements PresentableInterface{

	protected $softDelete = true;

	public function scopeCommonmember($query)
    {
        return $query->where('level', '=', 0);
    }

    public function scopeWechatmember($query)
    {
        return $query->where('level', '>', 0);
    }

	public function getPresenter()
    {
        return new MemberPresenter($this);
    }
}