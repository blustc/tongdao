<?php

use Robbo\Presenter\PresentableInterface;

Class Activity extends Eloquent{

	protected $softDelete = true;

	public function scopeTongdaoclub($query)
    {
        return $query->where('brand_id', '=', 0);
    }

    public function scopeBrandclub($query)
    {
        return $query->where('brand_id', '<>', 0);
    }

    public function scopeCommon($query)
    {
        return $query->where('status', '=', 0);
    }

}