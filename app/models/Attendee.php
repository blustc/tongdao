<?php

use Robbo\Presenter\PresentableInterface;

Class Attendee extends Eloquent{
	
	protected $softDelete = true;
	
	public function member()
    {
        return $this->belongsTo('Member');
    }

    public function activity()
    {
        return $this->belongsTo('Activity');
    }
}